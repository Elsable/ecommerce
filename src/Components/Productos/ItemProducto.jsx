import React, { Component } from 'react';
import { productos } from './Item';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Vermas } from './Vermas/Index';

export class ItemProducto extends Component {
	constructor(props) {
		super(props);
		this.state = {
			products: productos[0].seccion[0]
		};
	}
	render() {
		//    console.log(productos)
		// const header = (
		// 	<img alt="Card" src="https://www.primefaces.org/primereact/showcase/resources/demo/images/usercard.png" />
		// );
		// const footer = (
		// 	<span>
		// 		<center>
		// 			{/* <Button label="Vista Rapida" icon="pi pi-check" /> */}
		// 			<Vermas />
		// 		</center>
		// 		{/* <Button label="Cancel" icon="pi pi-times" className="p-button-secondary" /> */}
		// 	</span>
		// );

		const products = this.state.products;
		// console.log(products);
		if (products.length === 0) {
			return <div>No hay productos</div>;
		}

		let DatoRender = products.oferta.map((item, i) => (
			<div key={i} className="col-md-4">
				<div className="content-section implementation">
					<Card
						title={item.nombre + ' $' + item.precio}
						subTitle={item.Modelo}
						style={{ width: '360px' }}
						className="ui-card-shadow"
						footer={
							<span>
								<center>
								<Vermas info={item} />
								</center>
							</span>
						}
						header={<img alt="Card" src={item.imagen} />}
					>
						<div>{item.descripcion}</div>
					</Card>
				</div>
				<hr />
				<div className="clearfix" />
			</div>
		));

		let DatoRender2 = products.Descubre.map((item, i) => (
			<div key={i} className="col-md-4">
				<div className="content-section implementation">
					<Card
						title={item.nombre + ' $' + item.precio}
						subTitle={item.Modelo}
						style={{ width: '360px' }}
						className="ui-card-shadow"
						footer={
							<span>
								<center>
									<Vermas info={item} />
								</center>
							</span>
						}
						header={<img alt="Card" src={item.imagen} />}
					>
						<div>{item.descripcion}</div>
					</Card>
				</div>
				<hr />
				<div className="clearfix" />
			</div>
		));

		return (
			<div>
				<div className="row borde">
					<div className="offset-1 col-10">
						<div className="col-md-12">
							<h3>{'productos ofertas'}</h3>
							<div className="row">{DatoRender}</div>
						</div>
						<div className="col-md-12">
							<h3>{'productos descubre'}</h3>
							<div className="row">{DatoRender2}</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ItemProducto;
