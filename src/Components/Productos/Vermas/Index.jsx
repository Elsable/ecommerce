import React, { Component } from 'react'
import {Sidebar} from 'primereact/sidebar';
import {Button} from 'primereact/button';
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";



// const GlobalStyle = createGlobalStyle`
//     .p-sidebar-right{
//       width:"50%";
//     }
   
//     .clasemid{
//       width:"50%";
//     }
//     `

export class Vermas extends Component {

  constructor() {
    super();
    this.state = {
        visibleRight: false,
    };
}

render() {
    return (
        <div>
            {/* <GlobalStyle/> */}
            <div className="content-section implementation">   
                <Sidebar style={{ width:'50%'}}  visible={this.state.visibleRight} position="right" baseZIndex={1000000} onHide={(e) => this.setState({visibleRight: false})}>
                    <h1 style={{fontWeight:'normal'}}>{this.props.info.nombre}</h1>
                      {this.props.info.Modelo}
                      <br/>
                    <div className="row">
                    <div className="col-md-6">
                    <img alt="Card" style={{ width: 240 }}  src={this.props.info.imagen} /></div>
                    <div className="col-md-6">
                    <div className="container">
                    <h1 class="display-3">{this.props.info.precio} </h1>
                    </div>
                    </div>
                    </div>                    
                    <div>{this.props.info.descripcion}</div>
                    <Button type="button" onClick={(e) => this.setState({visibleRight: false})} label="Comprar" className="p-button-success"  style={{marginRight:'.25em'}} />
                    <Button type="button" onClick={(e) => this.setState({visibleRight: false})} label="Regresar" className="p-button-secondary"/>
                </Sidebar>                
                <Button label="Vista Rapida" icon="pi pi-check"  onClick={(e) => this.setState({visibleRight:true})} style={{marginRight:'.25em'}} />
            </div>
        </div>
    )
}
}

export default Vermas