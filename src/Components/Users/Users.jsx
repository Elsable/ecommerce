import React, { Component } from 'react';
import { OverlayPanel } from 'primereact/overlaypanel';
import { TabView, TabPanel } from 'primereact/tabview';
import { New_user } from './new_users/New_user';
import { Button } from 'primereact/button';
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";


const GlobalStyle = createGlobalStyle`
  #p-tabview-panels {
    padding:0;
    margin:0px;
  }`

export class Index2 extends Component {
	render() {
		return (
			<div>
        <GlobalStyle/>
				<Button type="button" label="Basic" onClick={(e) => this.op.toggle(e)} />

				<OverlayPanel ref={(el) => (this.op = el)}>
        <div className="col-md-12">
        <TabView>
						<TabPanel header="Registrate">
							<New_user />
						</TabPanel>
						<TabPanel header="Ingresar">Content II</TabPanel>
					</TabView>
        </div>
					
				</OverlayPanel>
			</div>
		);
	}
}

export default Index2;
