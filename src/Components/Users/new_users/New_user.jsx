import React, { Component } from 'react'
import {Password} from 'primereact/password';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';

export class New_user extends Component {
    constructor() {
        super();
        this.state = {
            value: null
        };
    }

    toggle() {
        this.setState({disabled: !this.state.disabled});
    }
  render() {
    return (
      <div className="row">        
        <span className="p-float-label">
            <InputText id="Nombre" type="text" size="15" value={this.state.value2} onChange={(e) => this.setState({value2: e.target.value})} />
            <label htmlFor="float-input">Nombre</label>
        </span><br/>
        <span className="p-float-label">
            <InputText id="Primer Apellido" type="text" size="15" value={this.state.value2} onChange={(e) => this.setState({value2: e.target.value})} />
            <label htmlFor="float-input">Primer Apellido</label>
        </span><br/>
        <span className="p-float-label">
            <InputText id="Segundo Apellido" type="text" size="15" value={this.state.value2} onChange={(e) => this.setState({value2: e.target.value})} />
            <label htmlFor="float-input">Segundo Apellido</label>
        </span><br/>
        <span className="p-float-label">
            <InputText id="Correo Electronico" type="text" size="15" value={this.state.value2} onChange={(e) => this.setState({value2: e.target.value})} />
            <label htmlFor="float-input">Correo Electronico</label>
        </span>
        <div>
            <div className="content-section implementation">
                <h5 className="first">Password</h5>
                <Password/>
                </div>
            </div>
            <Button label="Crear cuenta" />
      </div>
    )
  }
}

export default New_user
