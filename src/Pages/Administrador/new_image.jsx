import React, { Component } from 'react'

import {Growl} from 'primereact/growl';
import {FileUpload} from 'primereact/fileupload';;



export default class new_image extends Component {
  constructor() {
    super();
    
    this.onUpload = this.onUpload.bind(this);
    this.onBasicUpload = this.onBasicUpload.bind(this);
    this.onBasicUploadAuto = this.onBasicUploadAuto.bind(this);
  }

  onUpload(event) {
    this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded'});
  }

  onBasicUpload(event) {
    this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded with Basic Mode'});
  }

  onBasicUploadAuto(event) {   
    this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded with Auto Mode'});
  }

  render() {
    return (
      <div>        
           <div className="content-section implementation">
                    <h4>Agregar Imagenes</h4>
                    <FileUpload name="demo[]" url="./upload.php" onUpload={this.onUpload} 
                                multiple={true} accept="image/*" maxFileSize={1000000} />
                                
                   
                </div>
      </div>
    )
  }
}
