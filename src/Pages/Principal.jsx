import React, { Component } from 'react'
import Carrusel from '../Utils/Carrusel';
import ItemProducto from '../Components/Productos/ItemProducto';

export class Home extends Component {
  render() {
    return (
      <div className="col-md-12">
        <Carrusel/>
        <div >
            <ItemProducto/>
        </div>
      </div>
    )
  }
}

export default Home
