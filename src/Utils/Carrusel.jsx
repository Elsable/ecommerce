import React, { Component } from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
export class Carrusel extends Component {

  render() {
    return (
        <Carousel  autoPlay>
        <div >
            <img src="assets/1.jpg"  />
            <p className="legend">Legend 1</p>
        </div>
        <div>
            <img src="assets/2.jpg" />
            <p className="legend">Legend 2</p>
        </div>
        <div>
            <img src="assets/3.webp" />
            <p className="legend">Legend 3</p>
        </div>
        <div>
            <img src="assets/4.jpg" />
            <p className="legend">Legend 3</p>
        </div>
        <div>
            <img src="assets/5.jpg" />
            <p className="legend">Legend 3</p>
        </div>
    </Carousel>     
    
    )
  }
}

export default Carrusel
